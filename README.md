# Lightning Network Daemon (lnd) docker container
## Getting started
This project is a personnal initiative base on the source code of in https://github.com/lightningnetwork/lnd .

This docker container is a tiny **alpine/adm64** compatible container, automatically built from Official lnd team releases on https://github.com/lightningnetwork/lnd/releases.

## What is Lightning Network ?
The Lightning Network Daemon (`lnd`) is a complete implementation of a [Lightning Network](https://lightning.network) node.  `lnd` has several pluggable back-end chain services including [`btcd`](https://github.com/btcsuite/btcd) (a full-node), [`bitcoind`](https://github.com/bitcoin/bitcoin), and [`neutrino`](https://github.com/lightninglabs/neutrino) (a new experimental light client). The project's codebase uses the
[btcsuite](https://github.com/btcsuite/) set of Bitcoin libraries, and also exports a large set of isolated re-usable Lightning Network related libraries within it. 


### Configuration File
No configuration file is needed as you can add parameters to the "command" docker parameter. To see all options available, see reference file https://github.com/lightningnetwork/lnd/blob/master/sample-lnd.conf

If you wish to use a configuration file, change volume configuration to map your existing configuration file path.

### Persistance
A persistant volume is created on the fly, linked to /home/lnd/.lnd directory.

### How to use ?
You can use the sample docker-compose (v3) file below for **bitcoind** backend:

```yml
version: '3'
services:
  lnd:
    image: registry.gitlab.com/rofra/docker-lnd:latest
    container_name: lnd
    restart: unless-stopped
    stop_signal: SIGKILL
    build: ./
    ports:
      - "0.0.0.0:9735:9735"                               # Daemon Listener
      - "0.0.0.0:9911:9911"                               # Watchtower
      - "0.0.0.0:10009:10009"                             # RPC Server
      - "0.0.0.0:8080:8080"                               # gRPC Server
    command: [
      "--bitcoin.active",                                 # Connect to bitcoin network
      "--bitcoin.mainnet",                                # On mainnet
      "--debuglevel=debug",                               # Debug level
      "--bitcoin.node=bitcoind",                          # Bitcoind backend
      "--maxpendingchannels=10",                          # Max Pending channels
      "--bitcoind.rpcuser=api",                           # Your bitcoind RPC user
      "--bitcoind.rpcpass=api",                           # Your bitcoind RPC password
      "--bitcoind.zmqpubrawblock=tcp://127.0.0.1:28332",  # Your bitcoind ZMQ connections for raw blocks
      "--bitcoind.zmqpubrawtx=tcp://127.0.0.1:28333",     # Your bitcoind ZMQ connections for raw transactions
      "--alias=mynode",                                   # Alias of your Node
      "--externalip=2.3.4.5",                             # External IPV4 address
      "--color=#ffdc00",                                  # Lightning node color
      "--watchtower.active",                              # WatchTower enabled
      "--watchtower.externalip=2.3.4.5"                   # External IPV4 address
    ]
    volumes:
      - lnd-data:/data
volumes:
  lnd-data:

```

## Interracting with your container
### Starting lnd daemon
In order to start lnd you will need to have a local bitcoind node running in either mainnet, testnet or regtest mode.

Wait until bitcoind has synchronized with the testnet network **before** launching lnd.

Make sure that you do not have walletbroadcast=0 in your ~/.bitcoin/bitcoin.conf, or you may run into trouble. Notice that currently pruned nodes are not supported and may result in lightningd being unable to synchronize with the blockchain.

You can start lightningd with the following command:
```bash
docker-compose up -d
```

### How to use
#### Example: get all the commands availables in lncli
```bash
docker-compose exec lnd lncli -h
```
#### Example: get your lightning node infos
```bash
docker-compose exec lnd lncli getinfo
```
#### Example: unlock your wall
```bash
docker-compose exec lnd lncli unlock
```

## Releases
### On Git
We use **Github** for source versionning:
- [Sources](https://gitlab.com/rofra/docker-lnd)

### On Gitlab
We use **Gitlab** for compiled docker images:
- [Releases](https://gitlab.com/rofra/docker-lnd/container_registry/)

## Authors
* **Rodolphe Franceschi** - *Initial work* [linkedin](https://www.linkedin.com/in/rodolphe-franceschi-2a47b636/))

## Full GITLAB Modop
```bash
export TARGET=0.14.0-beta
# PRE ACTION
make prepare
# CHANGE in code the version
vi Dockerfile
git add Dockerfile
git commit -m "Dockerfile to ${TARGET}"
# ACTION
make build_and_test
make tag_local
make push_tags_gitlab
make push_image_gitlab
# POST ACTIONS
make merge_to_master
```

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.


