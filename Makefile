prepare:
	git branch "${TARGET}" || true
	git checkout "${TARGET}"
	
all: build_and_test tag_local push_tags_gitlab push_image_gitlab

merge_to_master:
	git checkout master
	git merge "${TARGET}"
	git push origin master
	
# SUB METHODS
build_and_test:
	# Local build
	docker-compose -f docker-compose.yml build --no-cache
	# Local build for tests
	docker-compose -f docker-compose.test.yml  build --no-cache
	# Run tests locally
	docker-compose -f docker-compose.test.yml run sut
	
tag_local:
	git tag -d v${TARGET} || true
	git tag v${TARGET}
	git tag -d latest
	git tag latest

push_tags_gitlab:
	git push --delete origin latest || true
	git push origin :v${TARGET} || true
	git push origin --tags

push_image_gitlab:
	docker login registry.gitlab.com -u rofra
	docker build -t registry.gitlab.com/rofra/docker-lnd:${TARGET} .
	docker push registry.gitlab.com/rofra/docker-lnd:${TARGET}
	docker build -t registry.gitlab.com/rofra/docker-lnd:latest .
	docker push registry.gitlab.com/rofra/docker-lnd:latest

